
    /**
     * Plugin Name: Simple Settings
     * Plugin URI: http://www.endseven.net
     * Description: JavaScript used to manipulate the admin interface.
     * Author: Jimmy K.
     * Version: 1.0
     * Author URI: http://www.endseven.net
     */

    jQuery(document).ready(function() {

        e7ss_updatePostBoxText();
        e7ss_addMoveToTrashConfirmationAlert();
        e7ss_initValuesMetaBox();

    });

    /**
     * Set the postbox container text values. This could be done in PHP by hooking
     * the 'gettext' action, but using jQuery has proven to be more reliable.
     * @author Jimmy K.
     */

    function e7ss_updatePostBoxText()
    {

        // Replace 'Publish' with 'Actions'..
        jQuery('#submitdiv h3.hndle span').html('Actions');

    }

    /**
     * Add a confirmation popup when clicking the 'Move to Trash' button.
     * @author Jimmy K.
     */

    function e7ss_addMoveToTrashConfirmationAlert()
    {

        jQuery('#major-publishing-actions #delete-action a').click(function(e) {

            // Ask for confirmation..
            $bConfirm = confirm('Are you sure?');

            if (!$bConfirm) {
                // Didn't confirm, prevent the click event..
                e.preventDefault();
            }

        });

    }

    /* ================================================== */
    /* Meta Box Functions
    /* ================================================== */

    /**
     * Initialize the 'Values' meta box.
     * @author Jimmy K.
     */

    function e7ss_initValuesMetaBox()
    {

        // Get the dropdown element..
        $oTypeElement = jQuery('#e7ss_type');

        if ($oTypeElement.length > 0) {

            $oTypeElement.change(function() {
                // Dropdown value changed, update the form elements visibility..
                e7ss_showValueElement(jQuery(this).attr('value'));
            });

            // Show the element for the selected type..
            e7ss_showValueElement($oTypeElement.attr('value'));

        }

    }

    /**
     * Hide all of the 'Values' form elements.
     * @author Jimmy K.
     */

    function e7ss_hideAllValueElements()
    {

        // Hide the elements wrapper..
        jQuery('.valueWrapper').css('display', 'none');

        // Hide the form elements..
        jQuery('.textareaValueWrapper').css('display', 'none');
        jQuery('.booleanValueWrapper').css('display', 'none');

    }

    /**
     * Show the specified 'Value' form element.
     * @author Jimmy K.
     */

    function e7ss_showValueElement($sType)
    {

        // Hide all the value elements.,
        e7ss_hideAllValueElements();

        if ($sType != '') {

            // Show the value wrapper..
            jQuery('.valueWrapper').css('display', '');

            if ($sType == 'textarea') {

                // Show the textarea..
                jQuery('.textareaValueWrapper').css('display', '');

            } else if ($sType == 'boolean') {

                // Show the radio group..
                jQuery('.booleanValueWrapper').css('display', '');

            }

        }

    }
