<?php

    class e7_SiteSettings {

        /**
         * Plugin Name: Simple Settings
         * Plugin URI: http://www.endseven.net
         * Description: A WordPress plugin to create, modify, and retrieve basic settings for use in WordPress themes, posts, and pages. This plugin also supports token replacement.
         * Author: Jimmy K.
         * Version: 1.0
         * Author URI: http://www.endseven.net
         */

        /* ================================================== */
        /* Variables
        /* ================================================== */

        // Hold our settings..
        public $aSettings = array();

        // The current admin notice text. We have to do it this
        // way because you can't pass arguments to the 'admin_notice'
        // hook. Bah!
        private $sAdminNoticeText = '';

        /* ================================================== */
        /* Constructor & Initialization
        /* ================================================== */

        /**
         * The constructor for this plugin.
         * @author Jimmy K.
         */

        public function __construct()
        {

            // Set our plugin settings..
            $this->aSettings['namespace'] = 'e7ss';
            $this->aSettings['dir'] = plugins_url('/e7-simple-settings');
            $this->aSettings['version'] = '1.0';

            // Initialize the goods..
            add_action('init', array($this, 'init'), 1);
            add_action('admin_init', array($this, 'initAdmin'), 1);
            add_action('shutdown', array($this, 'shutdown'), 1);

            // Enqueue the scripts and styles..
            add_action('wp_print_scripts', array($this, 'enqueueScripts'));

            // Keep the database happy and content..
            add_action('admin_head-edit.php', array($this, 'keepDatabaseHappy'));

            // Oh, you know this is clever! :P
            $this->aSettings['debug'] = (boolean) $this->getSetting('debug');

        }

        /**
         * Triggered when the plugin is initialized.
         * @author Jimmy K.
         */

        public function init()
        {

            // Start the output buffer (for token replacement)..
            $this->doHeaderActions();

            // Set the labels for our post type..
            $aLabels = array(
                'name' => __('Simple Settings', $this->aSettings['namespace']),
                'singular_name' => __('Simple Settings', $this->aSettings['namespace']),
                'add_new' => __('Add New', $this->aSettings['namespace']),
                'add_new_item' => __('Add New Setting', $this->aSettings['namespace']),
                'edit_item' => __('Edit Setting', $this->aSettings['namespace']),
                'view_item' => __('View Setting', $this->aSettings['namespace']),
                'search_items' => __('Search Settings', $this->aSettings['namespace']),
                'not_found' => __('No settings found!', $this->aSettings['namespace']),
                'not_found_in_trash' => __('No settings found in trash!', $this->aSettings['namespace']),
            );

            // Set the options for our post type..
            $aPostType = array(
                'labels' => $aLabels,
                'public' => true,
                'show_ui' => true,
                '_builtin' => false,
                'capability_type' => 'page',
                'hierarchical' => true,
                'rewrite' => false,
                'query_var' => $this->aSettings['namespace'],
                'supports' => array('title'),
                'show_in_menu' => true,
            );

            // Register our post type.
            register_post_type($this->aSettings['namespace'], $aPostType);

        }

        /**
         * Triggered when the plugin shuts down.
         * @author Jimmy K.
         */

        public function shutdown() {}

        /* ================================================== */
        /* Administration
        /* ================================================== */

        /**
         * Initialize the admin interface.
         * @see add_action
         * @author Jimmy K.
         */

        public function initAdmin()
        {

            // Add a metabox..
            add_meta_box('valuesMeta', 'Values', array($this, 'addValueMetaBox'), $this->aSettings['namespace'], 'normal', 'low');

            // Listen for save..
            add_action('save_post', array($this, 'saveSettings'));

            // Update the columns..
            add_action('manage_' . $this->aSettings['namespace'] . '_posts_columns', array($this, 'setCustomCols'));
            add_filter('manage_' . $this->aSettings['namespace'] . '_posts_custom_column', array($this, 'formatCustomCols'));

            // Update the messages..
            add_filter('post_updated_messages', array($this, 'setMessages'));

            // Swap default WordPress text with custom text..
            add_filter('gettext', array($this, 'setCustomText'), 1, 3);

        }

        /**
         * Enqueue the JavaScript and CSS.
         * @author Jimmy K.
         */

        public function enqueueScripts()
        {

            global $post_type;

            if ($post_type == $this->aSettings['namespace']) {

                // Enqueue the scripts..
                wp_register_script($this->aSettings['namespace'], $this->aSettings['dir'] . '/scripts.js');
                wp_enqueue_script($this->aSettings['namespace']);

                // Enqueue the styles..
                wp_register_style($this->aSettings['namespace'], $this->aSettings['dir'] . '/styles.css');
                wp_enqueue_style($this->aSettings['namespace']);

            }

            // Set the icons..
            // I wish we could do this in 'styles.css' but the directory name
            // would be unreliable. :(

            echo '
                <style type="text/css" media="all">

                    #menu-posts-' . $this->aSettings['namespace'] . ' .wp-menu-image {
                        background: url(\'' . $this->aSettings['dir'] . '/images/icons/plugin.png\') -20px 3px / 48px 22px no-repeat !important;
                    }

                    #menu-posts-' . $this->aSettings['namespace'] . '.wp-menu-open .wp-menu-image,
                    #menu-posts-' . $this->aSettings['namespace'] . ':hover .wp-menu-image {
                        /* Menu is open or being hovered over. */
                        background-position: 4px 3px !important;
                    }

                    #icon-edit.icon32-posts-' . $this->aSettings['namespace'] . ' {
                        background: url(\'' . $this->aSettings['dir'] . '/images/icons/plugin.png\') 2px 0 / 76px 38px no-repeat !important;
                    }

                </style>
            ';

        }

        /**
         * Replace sections of default WordPress text with something better.
         * @param string $sTranslatedText The translated text.
         * @param string $sUntranslatedText The untranslated text.
         * @param string $sDomain The text domain. (No idea what this does, tbh.)
         * @return string
         * @author Jimmy K.
         */

        public function setCustomText($sTranslatedText, $sUntranslatedText, $sDomain)
        {

            if ($sUntranslatedText == 'Move to Trash') {

                // Found the text we want to replace with something else..
                // Replace it with something more appropriate..
                return 'Move to Trash';

            }

            // Return the default translation..
            return $sTranslatedText;

        }

        /**
         * Set the messages.
         * @return array
         * @see add_action
         * @author Jimmy K.
         */

        public function setMessages()
        {

            // Set the messages for our post type..
            $aValues[$this->aSettings['namespace']] = array(
                0 => '', // Unused. Messages start at 1.
                1 => 'Setting updated! Keep up the good work, dude!',
                2 => 'Setting updated! Fantastic!',
                3 => 'Setting deleted. "Hasta la vista, baby!"',
                4 => 'Setting updated! Man, I love updating things!',
                5 => 'Revision? What revision?',
                6 => 'Setting saved. Right on!',
                7 => 'BOOM! Saved.',
                8 => 'Submitted? I guess so...',
                9 => 'Scheduled? I doubt it!',
                10 => 'Draft updated? I\'d like to think not!',
            );

            return $aValues;

        }

        /**
         * Add the 'value' meta box to the admin interface.
         * @see add_meta_box
         * @author Jimmy K.
         */

        public function addValueMetaBox()
        {

            global $post;

            // Get the values..
            $aMetaValues = get_post_custom($post->ID);
            $sType = $aMetaValues['type'][0];
            $sValue = $aMetaValues['value'][0];

            $sFormHTML = '
                <label>Type:</label>
                <div>
                    <select id="' . $this->aSettings['namespace'] . '_type" name="' . $this->aSettings['namespace'] . '_type" class="type">
                        <option value=""></option>
                        <option value="textarea" ' . ($sType == 'textarea' ? 'selected' : '') . '>Textarea</option>
                        <option value="boolean" ' . ($sType == 'boolean' ? 'selected' : '') . '>Boolean</option>
                    </select>
                </div>
                <div class="valueWrapper">
                    <label>Value:</label>
                    <div class="textareaValueWrapper">
                        <textarea cols="50" rows="5" name="' . $this->aSettings['namespace'] . '_textareaValue" class="value">' . $sValue . '</textarea>
                        <div class="clearer"></div>
                    </div>
                    <div class="booleanValueWrapper">
                        <label><input type="radio" name="' . $this->aSettings['namespace'] . '_booleanValue" value="true" ' . ($sValue == 'true' ? 'checked' : '') . ' /><span>True</span></label>
                        <label><input type="radio" name="' . $this->aSettings['namespace'] . '_booleanValue" value="false" ' . ($sValue == 'false' ? 'checked' : '') . ' /><span>False</span></label>
                        <div class="clearer"></div>
                    </div>
                </div>
            ';

            echo $sFormHTML;

        }

        /**
         * Save the settings.
         * @see add_action
         * @author Jimmy K.
         */

        public function saveSettings()
        {

            global $post;

            // Sanitize the type..
            $sType = $_POST[$this->aSettings['namespace'] . '_type'];
            $sType = stripslashes($sType);
            $sType = trim($sType);

            if ($sType == 'textarea') {
                $sValue = $_POST[$this->aSettings['namespace'] . '_textareaValue'];
            } else if ($sType == 'boolean') {
                $sValue = $_POST[$this->aSettings['namespace'] . '_booleanValue'];
            } else {
                $sValue = '';
            }

            // Sanitize the value..
            $sValue = $sValue;
            $sValue = stripslashes($sValue);
            $sValue = trim($sValue);

            // Sanitize the slug..
            $sSlug = $this->sanitizeSlug($post->post_title);

            // Update the post meta..
            update_post_meta($post->ID, 'type', $sType);
            update_post_meta($post->ID, 'value', $sValue);
            update_post_meta($post->ID, 'slug', $sSlug);

        }

        /* ================================================== */
        /* Custom Columns
        /* ================================================== */

        /**
         * Set the custom column data.
         * @param array $aDefaults The default column data.
         * @return array
         * @see add_action
         * @author Jimmy K.
         */

        public function setCustomCols($aDefaults)
        {

            // Remove the default 'date' column..
            unset($aDefaults['date']);

            // Add our custom column values..
            $aDefaults['slug'] = 'Slug';
            $aDefaults['type'] = 'Type';
            $aDefaults['value'] = 'Value';

            return $aDefaults;

        }

        /**
         * Format the custom column data.
         * @param string $sColumn The index of the column to format.
         * @return string
         * @see add_filter
         * @author Jimmy K.
         */

        public function formatCustomCols($sColumn)
        {

            global $post;

            switch (strtolower($sColumn)) {
            case 'slug':

                $aMetaValues = get_post_custom();

                $sValue = $aMetaValues['slug'][0];
                $sValue = htmlentities($sValue);
                $sValue = empty($sValue) ? '&nbsp;' : $sValue;

                echo $sValue;

                break;

            case 'type':

                $aMetaValues = get_post_custom();

                $sValue = $aMetaValues['type'][0];
                $sValue = htmlentities($sValue);
                $sValue = empty($sValue) ? '&nbsp;' : $sValue;

                echo $sValue;

                break;

            case 'value':

                $aMetaValues = get_post_custom();

                $sValue = $aMetaValues['value'][0];
                $sValue = htmlentities($sValue);
                $sValue = empty($sValue) ? '&nbsp;' : $sValue;

                echo $sValue;

                break;

            }

        }

        /**
         * Do the header actions!
         * @author Jimmy K.
         */

        public function doHeaderActions()
        {

            ob_start(array($this, 'doFooterActions'));

        }

        /**
         * Do the footer actions!
         * @return string
         * @author Jimmy K.
         */

        public function doFooterActions($sBuffer)
        {

            global $e7_SiteSettings;

            if (!is_admin()) {

                // Replace tokens on non-admin pages..
                $sReplacedBuffer = $e7_SiteSettings->replaceTokens($sBuffer);
                return $sReplacedBuffer;

            } else {

                // Don't replace tokens on the admin interface..
                return $sBuffer;

            }

        }

        /**
         * Set the admin notice.
         * @param string $sMessage The message to display.
         * @return string
         * @author Jimmy K.
         */

        private function setAdminNotice($sMessage) {

            $this->sAdminNoticeText = $sMessage;

        }

        /**
         * Show the admin notice.
         * @see add_action
         * @author Jimmy K.
         */

        public function showAdminNotice()
        {

            echo '<div class="updated"><p>';
            _e($this->sAdminNoticeText);
            echo '</p></div>';

            unset($this->sAdminNoticeText);

        }

        /* ================================================== */
        /* Helper Functions
        /* ================================================== */

        /**
         * Search and replace the token values for each setting.
         * @param string $sSubject The subject to search and replace tokens on.
         * @return string
         * @author Jimmy K.
         */

        protected function replaceTokens($sSubject)
        {

            // Get the published posts..
            $aPublishedPosts = get_posts(array(
                'post_type' => $this->aSettings['namespace'],
                'post_status' => 'publish',
                'posts_per_page' => 5000,
            ));

            foreach ($aPublishedPosts as $oPost) {

                // Get the meta values..
                $aMetaValues = get_post_custom($oPost->ID);

                // Replace the buffer..
                $sSubject = str_replace('{' . $aMetaValues['slug'][0] . '}', $aMetaValues['value'][0], $sSubject);

            }

            return $sSubject;

        }

        /**
         * Sanitize a slug.
         * @param string $sValue The value to sanitize.
         * @return string
         * @author Jimmy K.
         */

        private function sanitizeSlug($sValue)
        {

            $sValue = trim($sValue);
            $sValue = preg_replace("/[^A-Za-z0-9 _]/", '', $sValue);
            $sValue = strtolower($sValue);
            $sValue = str_replace(' ', '_', $sValue);

            return $sValue;

        }

        /**
         * Get a setting from the database.
         * @return string
         * @see e7_get_setting, get_setting
         * @author Jimmy K.
         */

        public function getSetting($sSlug = '')
        {

            // Sanitize the slug..
            $sSlug = $this->sanitizeSlug($sSlug);

            if (empty($sSlug)) {
                // Exit if the requested slug is empty..
                return '';
            }

            // Get the published posts..
            $aPublishedPosts = get_posts(array(
                'post_type' => $this->aSettings['namespace'],
                'post_status' => 'publish',
                'posts_per_page' => 5000,
            ));

            foreach ($aPublishedPosts as $oPost) {

                // Get the meta values..
                $aMetaValues = get_post_custom($oPost->ID);

                if ($sSlug == $aMetaValues['slug'][0]) {
                    // The requested slug matches this slug..
                    return $aMetaValues['value'][0];
                }

            }

            return '';

        }

        /**
         * Delete any posts that belong to this plugin and have the 'auto-draft'
         * status. This will help keep our database 'footprint' low. :)
         * @author Jimmy K.
         */

        public function keepDatabaseHappy()
        {

            // Get the garbage posts..
            $aGarbagePosts = get_posts(array(
                'post_type' => $this->aSettings['namespace'],
                'post_status' => 'auto-draft',
                'posts_per_page' => 99999, // Arbitrary value..
            ));

            $i = 0;

            foreach ($aGarbagePosts as $oPost) {

                // Forcefully this post because it's, well, garbage..
                wp_delete_post($oPost->ID, true);
                $i++;

            }

            if ($this->aSettings['debug'] && $i > 0) {

                // Output a debug notice..
                $this->sAdminNoticeText = $i . ' garbage posts deleted.';
                add_action('admin_notices', array($this, 'showAdminNotice'));

            }

            // There is a weird bug where if you go to add a new setting,
            // insert a title, set the values, then change the title, the
            // slug that's generated is for the OLD title instead of the
            // current title. So! We're going to loop through the settings
            // and update any mismatching slugs.

            // Get all the posts..
            $aAllPosts = get_posts(array(
                'post_type' => $this->aSettings['namespace'],
                'post_status' => 'publish',
                'post_per_page' => 99999, // Arbitrary value..
            ));

            foreach ($aAllPosts as $oPost) {

                // Get the meta values..
                $aMetaValues = get_post_custom($oPost->ID);

                // Sanitize the title-based slug..
                $sSlugFromTitle = $this->sanitizeSlug($oPost->post_title);

                if ($sSlugFromTitle != $aMetaValues['slug'][0]) {

                    // Title-based slug and stored slug don't match; update..
                    update_post_meta($oPost->ID, 'slug', $sSlugFromTitle);

                }

            }

        }

    }

    // Create an instance of our plugin object.
    $e7_SiteSettings = new e7_SiteSettings();

    /**
     * Local scope function to get a setting from the database.
     * @return string
     * @author Jimmy K.
     */

    function ss_get_setting($sSlug)
    {

        global $e7_SiteSettings;

        // Get the setting.
        $sValue = $e7_SiteSettings->getSetting($sSlug);

        return $sValue;

    }

    if (!function_exists('get_setting')) {

        /**
         * Local scope helper function to get a setting from the database.
         * @return string
         * @author Jimmy K.
         */

        function get_setting($sSlug)
        {

            return ss_get_setting($sSlug);

        }

    }
